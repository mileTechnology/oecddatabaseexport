# OECDDatabaseExport

Tool to download timeseries data from OECD database via the official, public API.
Additionally, the tool converts data into standard CSV/XLS format for directly usage in math tools like R, Mathlab, and others.